var pageCounter = 1;
var verseContainer = document.getElementById("bible-verse");
var btn = document.getElementById("btn");
btn.addEventListener("click", function () {

    var ourRequest = new XMLHttpRequest();
    ourRequest.open('GET', 'https://api.myjson.com/bins/g5mue');
    ourRequest.onload = function () {
        var ourData = JSON.parse(ourRequest.responseText);
        renderHTML(ourData);
    };
    ourRequest.send();
});

function renderHTML(data) {
    var htmlString = "";
    for (i = 0; i < data.length; i++) {
        htmlString += "<p>" + data[i].text + "</p>"
    }
    verseContainer.insertAdjacentHTML('beforeend', htmlString)
}