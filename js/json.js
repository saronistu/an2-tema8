[
    {
        "book": "Ioan",
        "chapter": "1",
        "verse": "1",
        "text": "La inceput era Cuvantul, si Cuvantul era cu Dumnezeu si Cuvantul era Dumnezeu."
    },
    {
        "book": "Geneza",
        "chapter": "1",
        "verse": "1",
        "text": "La inceput Dumnezeu a facut cerurile si pamantul."
    },
    {
        "book": "Iov",
        "chapter": "1",
        "verse": "1",
        "text": "Era un om in tara Ut, si numele lui era Iov."
    }
]